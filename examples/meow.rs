// twink, a little logging crate
// Copyright (c) 2023 fawn and rini
//
// SPDX-License-Identifier: Apache-2.0

fn main() {
    twink::purr!("meowed <cyan>{}</> times", 999999);
    twink::mrrr!("catnip running <b><red>out</>");
    twink::hiss!("<i>your time has come</>");
    twink::nyaa!("qhar");
}
