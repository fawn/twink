use crate::{hiss, meow, mrrr, nyaa, purr};
use log::{kv, Level, Log};

pub use log::LevelFilter;

struct TwinkVisitor;

impl<'k> kv::Visitor<'k> for TwinkVisitor {
    fn visit_pair(&mut self, key: kv::Key<'k>, value: kv::Value<'k>) -> Result<(), kv::Error> {
        meow!("    <bold>{}</> {}", key, value);
        Ok(())
    }
}

struct TwinkLogger;

impl Log for TwinkLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        metadata.level() <= log::max_level()
    }

    fn log(&self, record: &log::Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        match record.level() {
            Level::Error => hiss!("<bold>{}</> {}", record.target(), record.args()),
            Level::Warn => mrrr!("<bold>{}</> {}", record.target(), record.args()),
            Level::Info => purr!("<bold>{}</> {}", record.target(), record.args()),
            _ => nyaa!("<bold>{}</> {}", record.target(), record.args()),
        }

        record.key_values().visit(&mut TwinkVisitor).unwrap();
    }

    fn flush(&self) {}
}

pub fn setup() {
    setup_level(LevelFilter::Info);
}

pub fn setup_level(level: LevelFilter) {
    log::set_logger(&TwinkLogger).unwrap();
    log::set_max_level(level);
}
